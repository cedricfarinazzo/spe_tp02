#include <stdio.h>
#include "digit_count.h"

int main()
{
    printf("digit_count(%lu) = %d\n", ((unsigned long)0), digit_count(0));

    unsigned long digit = 1;
    for (unsigned long i = 0; i <= 63; ++i)
    {
        printf("digit_count(%lu) = %d\n", digit, digit_count(digit));
        digit *= 2;
    }
    return 0;
}
