#include <stdlib.h>

unsigned char digit_count(unsigned long n)
{
    if (n == 0)
    {
        return 1;
    }
    unsigned char count = 0;
    while (n != 0)
    {
        n /= 10;
        ++count;
    }
    return count;
}
