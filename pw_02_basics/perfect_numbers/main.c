#include <stdio.h>
#include "is_perfect_number.h"
#include "divisor_sum.h"

int main()
{
    for (unsigned long i = 1; i <= 100000; ++i)
    {
        if (is_perfect_number(i) != 0)
        {
            printf("%lu\n", i);
        }
    }
    return 0;
}
