#include <stdlib.h>
#include <stdio.h>

unsigned long divisor_sum(unsigned long n)
{
    if (n == 1)
    {
        return 1;
    }
   unsigned long sum = 1;
   unsigned long i = n % 2 == 0 ? 2 : 3;
   for (; i*i< n; i+=2)
   {
       sum += ((n % i) == 0) ? i + (n / i): 0;
   }

   sum += i*i == n ? i : 0;

   return sum;
}
