unsigned long facto(unsigned long n)
{
    unsigned long result = 1;
    for (unsigned long i = 2; i <= n; ++i)
    {
        result *= i;
    }
    return result;
}
