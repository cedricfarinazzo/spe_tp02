#include <stdlib.h>

unsigned long fibo(unsigned long n)
{
   if (n <= 2)
   {
       return n;
   }
   else
   {
       unsigned long f1 = 0; 
       unsigned long f2 = 1;
       
       for (unsigned long i = 2; i <= n; ++i)
       {
            unsigned long f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
       }

       return f2;
   }
}
